# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

from mywebsite_blog import models as blogmodels
from mywebsite_home import views as homeviews


class NewsListView(homeviews.NewsListView):
    pass

    def get_context_data(self, **kwargs):
        context = super(NewsListView, self).get_context_data(**kwargs)
        # add extra context
        # context["extra_article"] = blogmodels.Article.objects.active_translations().get(
        #     slug__iexact='extra_article')
        return context
