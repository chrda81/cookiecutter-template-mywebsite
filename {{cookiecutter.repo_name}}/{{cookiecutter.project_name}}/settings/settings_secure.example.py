# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------

Django settings for website project.

This file contains settings, which should not be shared with others!

For more information on this file, see
https://docs.djangoproject.com/en/2.0/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/2.0/ref/settings/
"""

import os

from {{ cookiecutter.project_name }}.settings.base import BASE_DIR, DJANGO_PROJECT_NAME

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '<your secret key goes in here>'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
FORCE_SCRIPT_NAME = ''
ALLOWED_HOSTS = []


# Database
# https://docs.djangoproject.com/en/2.0/ref/settings/#databases

DATABASES = {
    # 'default': {
    #     'ENGINE': 'django.db.backends.mysql',
    #     'NAME': '<database>',
    #     'USER': '<username>',
    #     'PASSWORD': '<password>',
    #     'HOST': '<host>',
    #     'PORT': '3306',
    #     'OPTIONS': {
    #         'autocommit': True,
    #     },
    # }
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}


# Main FileBrowser Directory
# https://django-filebrowser.readthedocs.io/en/latest/settings.html#settings

FILEBROWSER_DIRECTORY = '{{ cookiecutter.template_dir }}/'


# Email backend
# https://hellowebbooks.com/news/tutorial-setting-up-a-contact-form-with-django/

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
DEFAULT_FROM_EMAIL = 'postmaster@example.com'
DEFAULT_TO_EMAIL = 'testing@example.com'
DEFAULT_SUBJECT = 'New message from submission'
EMAIL_HOST_USER = ''
EMAIL_HOST_PASSWORD = ''
EMAIL_USE_TLS = False
EMAIL_PORT = 1025


# Social Media App Settings
# Facebook

SOCIAL_AUTH_USE_FACEBOOK = False
SOCIAL_AUTH_FACEBOOK_KEY = '<your app id>'  # App ID
SOCIAL_AUTH_FACEBOOK_SECRET = '<your app secret>'  # App Secret


# Caching translations and other objects with Memcache
# https://realpython.com/python-memcache-efficient-caching/#installing-memcached
# https://django-parler.readthedocs.io/en/latest/performance.html

PARLER_ENABLE_CACHING = True
SITES_ENALBE_CACHING = True
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'KEY_PREFIX': 'mysite.production',  # Change this
        'LOCATION': '127.0.0.1:11211',
        'TIMEOUT': 24*3600  # cache data for 24 hours, then fetch them once again
    },
}


# Define custom filter attributes for extending the logging formatter
def add_custom_filter(record):
    record.project = DJANGO_PROJECT_NAME
    return True


# DBBackup Settings
# https://github.com/django-dbbackup/django-dbbackup
# use full path here, because cron cannot handle relative paths

LOGFILE = os.path.join(BASE_DIR, 'scheduled_django_jobs.log')
DBBACKUP_LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'add_custom_filter': {
            '()': 'django.utils.log.CallbackFilter',
            'callback': add_custom_filter,
        }
    },
    'handlers': {
        'file': {
            'formatter': 'base',
            'class': 'logging.FileHandler',
            'filters': ['add_custom_filter'],
            'filename': LOGFILE,
        },
    },
    'formatters': {
        'base': {'format': '%(asctime)s.%(msecs)03d  %(project)s  %(message)s', 'datefmt': '%Y-%m-%d %H:%M:%S'},
    },
    'loggers': {
        'dbbackup': {
            'handlers': [
                'file'
            ],
            'level': 'INFO',
            'propagate': True,
        },
    }
}
# - For the dropbox settings usage, please use "pip install dropbox" before
# DBBACKUP_STORAGE = 'storages.backends.dropbox.DropBoxStorage'
# DBBACKUP_STORAGE_OPTIONS = {
#     'oauth2_access_token': '<your-secret-dropbox-auth-token>',
#     'root_path': '/Backups'
# }
DBBACKUP_STORAGE = 'django.core.files.storage.FileSystemStorage'
DBBACKUP_STORAGE_OPTIONS = {'location': '/var/backups'}


# Settings for django-crontab
# extend PATH with location for 'mysqldump'

CRONTAB_COMMAND_PREFIX = 'PATH=$PATH:/usr/local/bin'
CRONTAB_COMMAND_SUFFIX = '2>&1'
CRONTAB_DJANGO_PROJECT_NAME = DJANGO_PROJECT_NAME
CRONJOBS = [
    ('0 */4 * * *', 'django.core.management.call_command', ['dbbackup', '-c', '-z']),
    ('5 */4 * * *', 'django.core.management.call_command', ['mediabackup', '-c', '-z']),
]


if not DEBUG:
    WEBPACK_LOADER['DEFAULT'].update({
        'CACHE': True,
        # 'BUNDLE_DIR_NAME': 'dist/',
        # 'STATS_FILE': os.path.join(BASE_DIR, 'webpack-stats-prod.json')
    })
