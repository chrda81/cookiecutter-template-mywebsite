# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

from django.urls import path
from mywebsite_home import views

from . import home_views as custom_views

app_name = 'home'

urlpatterns = [
    path('', custom_views.NewsListView.as_view(), name='index'),
    path('set_language', views.set_language, name='set_language'),
    path('search', views.SearchListView.as_view(), name='search'),
    path('about-me', views.about_me, name='about-me'),
    path('crowdfunding', views.crowdfunding, name='crowdfunding'),
    path('disclosures', views.disclosures, name='disclosures'),
    path('data-privacy', views.data_privacy, name='data-privacy'),
    path('contact', views.contact, name='contact'),
    path('terms-and-conditions', views.terms_and_conditions, name='terms-and-conditions'),
]
