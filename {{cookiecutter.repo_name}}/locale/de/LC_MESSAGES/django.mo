��    Q      �  m   ,      �     �     �               /     A     Q     f     ~     �     �     �     �     �     �     �       	     
        '     =     O     ^     f     v     }     �     �  
   �  	   �     �     �     �     �     �     	  	   	     	  !   ,	     N	     U	  	   ^	     h	     q	     �	     �	     �	  	   �	  	   �	  
   �	  
   �	     �	     �	       
  &   !
     H
     X
     s
     �
     �
     �
     �
     �
     �
  "   �
  0     >   F  	   �     �     �     �     �     �     �     �     �     �     �          &  k  .     �     �     �  
   �     �     �  #   �               /     7  
   >     I     [    k  	   �     �  
   �  
   �     �     �     �     �                    0     5     :     @     E  
   M     X     `     m     y     �  	   �     �     �     �  	   �     �     �  9   �  j        �     �     �     �     �     �  +   �  +   �  *   �     (     F     Y     p  C   �     �     �     �       :     [   K  ]   �  	                  "  
   +     6     B     K     R     ^  	   d     n  
   �           K   E   >   .   &   %   $      ,       7   @   O       :      2   !   '      8                    1                               B   (   #         <   D       P   
   *      =             3       5       H                    A       M      F       G   "   Q   /   6          L                             0   ;   )       ?       I      9       C   -   N   +   4       J           	                 ARTICLE_COMPARE ARTICLE_INFO_DETAILS ARTICLE_INFO_HITS ARTICLE_LIST_READ_MORE ARTICLE_READ_TIME ARTICLE_SHARING ARTICLE_SHARING_BODY ARTICLE_SHARING_SUBJECT Advertising Item CONTACT_HEADING CONTACT_SUBMIT COOKIE_INFO_CLOSE COOKIE_INFO_LEGAL_ADVICE COOKIE_INFO_OK COOKIE_INFO_PARA Comment Compare %(name)s Date/time Dictionary Django administration Django site admin Download album English GALLERY_HEADING German Hide public albums Home List MENU_ABOUT MENU_BLOG MENU_BLOG_ARTICLES MENU_BLOG_CATEGORIES MENU_CONTACT MENU_CROWDFUNDING MENU_DATA_PRIVACY MENU_GALLERY MENU_HOME MENU_IMPRINT Main manual for this application. Manual MediaURL Menu Link Meta Tag NOSCRIPT_HEADER NOSCRIPT_INFO_REF NOSCRIPT_INFO_TEXT Object PAGE_NEXT PAGE_PREV PAGE_TEXT1 PAGE_TEXT2 Pictures Please correct the error below. Please correct the errors below. Results for <strong>%(query)s</strong> Revert %(name)s SEARCH_GALLERY_PLACEHOLDER SEARCH_INFO_PARA SEARCH_INFO_PLACEHOLDER SEARCH_NORESULTS SEARCH_RESULTS_HEADING See all albums Show public albums Slider Item Sorry, no albums match your query. Sorry, you aren't authorized to view any albums. Sorry, you aren't authorized to view any photos in this album. StaticURL String Translation User WEBSITE_BACK_TO_TOP compare contact_captcha contact_email contact_message contact_name minutes one photo %(num_photos)s photos seconds Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-10-15 11:34+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Artikel-Änderungsverlauf Details Treffer mehr lesen Lesezeit Artikel teilen Ich fand diesen Artikel interessant Geteilter Artikel: Advertising Item Kontakt Senden Schließen Was sind Cookies? Ok, verstanden. Für statistische Zwecke und um bestmögliche Funktionalität zu bieten, speichert diese Website Cookies auf Ihrem Gerät. Das Speichern von Cookies kann in den Browser-Einstellungen deaktiviert werden. Wenn Sie die Website weiter nutzen, stimmen Sie der Verwendung von Cookies zu. Kommentar Vergleichen von %(name)s Datum/Zeit Dictionary Django Administration Django site admin Album herunterladen English Gallerie Deutsch Verstecke öffentliche Alben Home List Über Blog Artikel Kategorien Kontakt Crowdfunding Datenschutz Gallerie Home Impressum Das Handbuch zur Applikation. Handbuch MediaURL Menu Link Meta Tag Warnung Anleitung wie Sie JavaScript in Ihrem Browser einschalten Um den vollen Funktionsumfang dieser Webseite zu erfahren, benötigen Sie JavaScript. Hier finden Sie die  Objekt Vor Zurück Seite von Bilder Bitte korrigiere den untenstehenden Fehler. Bitte korrigiere den untenstehenden Fehler. Ergebnisse für <strong>%(query)s</strong> Wiederherstellen von %(name)s Gallerie-Suche ... Suche Artikel/News ... Suche Artikel/News ... Es wurden keine Artikel mit dem angegebenen Suchparameter gefunden. Suchergebnisse Zeige alle Alben Zeige öffentliche Alben Slider Item Entschuldigung, es gibt keine Alben zu deinem Suchbegriff. Entschuldigung, aber du bist nicht authorisiert, um irgendwelche Alben einsehen zu können. Entschuldigung, aber du bist nicht authorisiert, um Bilder dieses Albums einsehen zu können. StaticURL String Translation Benutzer Zum Anfang Vergleichen Captcha: eMail: Mitteilung: Name: Minute(n) Ein Bild %(num_photos)s Bilder Sekunde(n) 