#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

import logging
import os
import pipes
import subprocess
import sys

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger("post_gen_project")

PROJECT_DIR = os.getcwd()  # the new project folder {{ cookiecutter.repo_name }}


def create_virtualenv():
    logger.info("Creating virtualenv '{{ cookiecutter.virtualenv }}' ..")
    cmd = "source /usr/local/bin/virtualenvwrapper.sh && mkvirtualenv " + \
        pipes.quote('{{ cookiecutter.virtualenv }}')
    ret = subprocess.call(cmd,
                          executable='bash',
                          stderr=subprocess.STDOUT,
                          shell=True
                          )

    if ret != 0:
        exit(1)


def create_env_file():
    env_file = os.path.join(PROJECT_DIR, ".env")
    logger.info("Creating env_file %s ..", env_file)
    f = open(env_file, "w")
    f.write("workon {{ cookiecutter.virtualenv }}\n")
    f.close()


def create_vscode_file():
    vscode_file = os.path.join(PROJECT_DIR, ".vscode", "settings.json")
    logger.info("Creating vscode file %s ..", vscode_file)
    f = open(vscode_file, "w")
    f.write("""
{
    "python.pythonPath": "/Users/chris/Envs/{{ cookiecutter.virtualenv }}/bin/python",
    "python.linting.pep8Path": "/Users/chris/Envs/{{ cookiecutter.virtualenv }}/bin/pycodestyle",
    "python.linting.pep8Args": [
        "--max-line-length=119"
    ],
    "python.linting.pep8Enabled": true,
    "python.formatting.autopep8Path": "/Users/chris/Envs/{{ cookiecutter.virtualenv }}/bin/autopep8",
    "python.formatting.autopep8Args": [
        "--max-line-length=119"
    ],
    "python.linting.pylintPath": "/Users/chris/Envs/{{ cookiecutter.virtualenv }}/bin/pylint",
    "python.linting.pylintArgs": [
        "--errors-only",
        "--load-plugins",
        "pylint_django"
    ],
    "python.linting.pylintEnabled": true,
    "eslint.enable": false,
    "html.format.enable": false,
    "editor.formatOnSave": false,
    "editor.rulers": [
        119,
        140
    ],
    "files.exclude": {
        "**/.git": true,
        "**/.svn": true,
        "**/.hg": true,
        "**/CVS": true,
        "**/.DS_Store": true,
        ".vscode": false,
        ".idea": true,
        "**/*,pyc": true,
        "*/__pycache__": true
    },
    "workbench.editor.enablePreview": false,
    "editor.find.globalFindClipboard": true,
    "search.globalFindClipboard": true,
    "editor.minimap.side": "left",
    "git.promptToSaveFilesBeforeCommit": true,
    "markdown-pdf.type": [
        "html",
      ],
    "markdown-pdf.convertOnSave": true,
    "markdown-pdf.outputDirectory": "templates/{{ cookiecutter.virtualenv }}/admin_doc",
    "markdown-pdf.convertOnSaveExclude": [
        "^(?!MANUAL.*\\.md$)",
    ],
    "markdown-pdf.includeDefaultStyles": false,
    "markdown-pdf.styles": [
        "static/css/bootstrap.min.css",
    ],
}
    """)
    f.close()


def create_project_folders():
    logger.info("Creating project folders ..")
    os.makedirs(os.path.join(PROJECT_DIR, "media", "{{ cookiecutter.template_dir }}"), exist_ok=True)


if __name__ == "__main__":
    create_virtualenv()
    create_env_file()
    create_vscode_file()
    create_project_folders()
